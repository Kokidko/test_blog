<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'title' => 'Php is awesome',
                'slug' => 'php_is_awesome',
                'body' => 'Article about Php language. Article about Php language. Article about Php language.Article about Php language. Article about Php language. Article about Php language.Article about Php language. Article about Php language. Article about Php language.'
            ],
            [
                'title' => 'Html is awesome',
                'slug' => 'html_is_awesome',
                'body' => 'Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.'
            ],
            [
                'title' => 'CSS is awesome',
                'slug' => 'css_is_awesome',
                'body' => 'Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.'
            ],
            [
                'title' => 'Php is awesome',
                'slug' => 'php_is_awesome',
                'body' => 'Article about Php language. Article about Php language. Article about Php language.Article about Php language. Article about Php language. Article about Php language.Article about Php language. Article about Php language. Article about Php language.'
            ],
            [
                'title' => 'Html is awesome',
                'slug' => 'html_is_awesome',
                'body' => 'Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.Article about HTML.'
            ],
            [
                'title' => 'CSS is awesome',
                'slug' => 'css_is_awesome',
                'body' => 'Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.Article about CSS.'
            ]
        ]);
    }
}
