@extends('main')

@section('main_content')

        <div class="card mb-3" >
            <div class="card-body">
                <hr>
                <h1 class="display-4 font-italic">{{$post->title}}</h1>
                <p class="card-text">{{$post->body}}</p>
                <hr>

                <div class="col-md-12" id="cerror">
                    <h3>Add a comment:</h3>
                    <p>Fields marked as * is required</p>
                    @include('errors')
                    <form action="/posts/{{$post->slug}}/comments" method="post" id="comments">

                        <input type="hidden" id="csrf" value="{{csrf_token()}}" />
                        <div class="form-group">
                            <label for="name">Name* </label>
                            <input class="form-control" type="text" name="name" id="name">
                        </div>

                        <div class="form-group">
                            <label for="email">Email* </label>
                            <input class="form-control" type="text" name="email" id="email">
                        </div>

                        <div class="form-group">
                            <label for="body">Enter your comment*</label>
                            <textarea name="body" id="body"  class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-success" id="sendComment">Post comment</button>
                        </div>

                    </form>
                </div>

                <div class="comments col-md-12">
                    <h3>Comments (<span id="count">{{count($post->comments)}}</span>):</h3>
                    @foreach($post->comments as $comment)
                        <div>
                            <p>{{$comment->name}}; {{$comment->email}}</p>
                            <p><strong>{{$comment->body}}</strong></p>
                        </div>
                    @endforeach
                    <div class="currentComment"></div>
                </div>

            </div>
        </div>

@endsection