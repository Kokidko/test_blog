@extends('main')

@section('main_content')



    <div class="card mb-3" >
        <div class="card-body">

            <form action="/posts/{{$post->slug}}" method="post">

                @include('errors')

                {{csrf_field()}}

                {{method_field('put')}}

                <div class="form-group">
                    <label for="title">Title:</label>
                    <input class="form-control" value="{{$post->title}}" type="text" name="title" id="title">
                </div>

                <div class="form-group">
                    <label for="body">Body:</label>
                    <textarea class="form-control" name="body" id="body">{{$post->body}}</textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Update</button>
                </div>

            </form>



        </div>
    </div>

@endsection