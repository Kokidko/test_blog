@extends('main')

@section('main_content')



    <div class="card mb-3" >
        <div class="card-body">

            <form action="/posts" method="post">

                @include('errors')

                {{csrf_field()}}

                <div class="form-group">
                    <label for="title">Title:</label>
                    <input class="form-control" value="{{old('title')}}" type="text" name="title" id="title">
                </div>

                <div class="form-group">
                    <label for="slug">Slug:</label>
                    <input class="form-control" value="{{old('slug')}}" type="text" name="slug" id="slug">
                </div>

                <div class="form-group">
                    <label for="body">Body:</label>
                    <textarea class="form-control" name="body" id="body">{{old('body')}}</textarea>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Create</button>
                </div>

            </form>



        </div>
    </div>

@endsection