@extends('main')

@section('main_content')
    <div class="mb-3" >
        <h1 class="display-4 font-italic">Test blog</h1>
    </div>
<div class="main">
    @foreach($posts as $post)
        <div class="card mb-3" >
            <div class="card-body">
                <h5 class="card-title">{{$post->title}}</h5>
                <p class="card-text">{{str_limit($post->body, 300)}}<a href="posts/{{$post->slug}}" class="card-link">Show more</a></p>
                <div class="text-right">
                    <a href="posts/{{$post->slug}}/edit" class="btn btn-warning card-link">Edit</a>
                    <form class="d-inline-block" action="posts/{{$post->slug}}" method="post">
                        {{csrf_field()}}
                        {{method_field('delete')}}
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection