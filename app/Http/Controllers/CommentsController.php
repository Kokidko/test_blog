<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function store(Post $post){
        $this->validate(request(),[
            'name' => 'required|min:2',
            'email' => 'required|email',
            'body' => 'required|max:255'
        ]);
        $obj = $post->addComment(
            request()->post('name'),
            request()->post('email'),
            request()->post('body'));

       return json_encode([
           'name'=>$obj->name,
           'email'=>$obj->email,
           'body'=>$obj->body,
       ]);
    }
}
