<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'slug', 'body'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function addComment($name, $email, $body){
        return Comment::create([
            'name' => $name,
            'email' => $email,
            'body' => $body,
            'post_id' => $this->id
        ]);

    }

}
