$(function() {

    $('#comments').validate({
        rules: {
            email:{
                required: true,
                email: true
            },
            name: "required",
            body: "required",

        }
    });

    $('#comments').on('submit', function (e) {
        e.preventDefault();
        var form = $(this),
            method = form.attr('method'),
            name = form.find('#name').val(),
            email = form.find('#email').val(),
            body = form.find('#body').val(),
            data = {
                name: name,
                email: email,
                body: body
            };



        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#csrf').val()
            }
        });
        $.ajax({
            url: form.attr('action'),
            type: method,
            data: data,
            dataType: 'json',
            success: function(result) {
                console.log(result);
                $('#count').text(1 + parseInt($('#count').text()));
                var listItem = document.createElement('p');
                listItem.innerHTML = "<p>" + result.name + "; " + result.email +  "</p><p><strong>" + result.body + "</strong></p>";
                $('.currentComment').prepend(listItem);
            }
        });

        $('form[id=comments]').trigger('reset');

    });
});